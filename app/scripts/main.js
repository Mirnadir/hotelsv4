$(document).ready(function(){
    // games guide start
    $('.games__guide li:first-child').addClass('active');
    $('.link-item').hide();
    $('.link-item:first').show();

    $('.games__guide li').click(function(){
    $('.games__guide li').removeClass('active');
    $(this).addClass('active');
    $('.link-item').hide();
    
    var activeLink = $(this).find('a').attr('href');
    $(activeLink).fadeIn();
    return false;
    });
    // games guide end

    // mobile menu start
    $('.menu-button').click(function() {
        $('.mobile-item').slideToggle(300);
    })
    // mobile menu end 
})
